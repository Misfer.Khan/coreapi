﻿using coreAPI.Models;
using System.Collections;
using System.Collections.Generic;

namespace coreAPI.Data 
{
    public interface ICommanderRepo 
    {
        IEnumerable<Command> GetAppCommands();
        Command GetCommandById(int Id);
    }
}