﻿using coreAPI.Models;
using System.Collections.Generic;
using System.Net.Http.Headers;

namespace coreAPI.Data 
{
    public class MockCommanderRepo : ICommanderRepo
    {
        public IEnumerable<Command> GetAppCommands()
        {
            var commands = new List<Command> 
            {
             new Command { Id = 0, HowTo = "Boil an Egg", Line = "Boil Water", Platform = "Kettle" },
             new Command { Id = 1, HowTo = "Cut Bread", Line = "Knife", Platform = "Cutting Board" },
             new Command { Id = 2, HowTo = "Make Coffee", Line = "Boil Water", Platform = "Kettle" }
        };
            return commands;
        }

        public Command GetCommandById(int Id)
        {
            return new Command { Id = 0, HowTo = "Boil an Egg", Line = "Boil Water", Platform = "Kettle" };
        }
    }
}