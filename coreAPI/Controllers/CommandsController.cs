﻿using coreAPI.Data;
using coreAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections;
using System.Collections.Generic;

namespace coreAPI.Controllers
{
    [Route("api/commands")]
    //wildcard implementation [Route("api/[controller]")]
    [ApiController]
    public class CommandsController : ControllerBase
    {
        private readonly ICommanderRepo _repository;

        //Constructor for Handling the DI
        public CommandsController(ICommanderRepo repository)
        {
            _repository = repository;
        }

        // GET api/commands
        [HttpGet]
        public ActionResult<IEnumerable<Command>> GetAllCommands() 
        {
            var commandItems = _repository.GetAppCommands();
            return Ok(commandItems);
        }

        // GET api/commands/{id}
        [HttpGet("{id}")]
        public ActionResult<Command> GetCommandById(int id) 
        {
            var commandItem = _repository.GetCommandById(id);
            return Ok(commandItem);
        }

    }
}
